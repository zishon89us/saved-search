# Saved Search for multi-listings #

This document contains the architecure diagram & working description in points to show the solution for saved searches service triggering alerts on new listings.

### Architecture Diagram ###

![Saved Search Dubizzle Architecture Diagram](saved-search-v1.jpg "Saved Search Dubizzle Architecture Diagram")

### Fascinating Points ###

- System is architectured as miscroservices, it utilizes pub/sub event mechanism to accommodate scalability. It is open to select Apache Kafka, AWS Kinesis, RabbitMQ or so depending on the clarification. This approach is used when system is expected to handle huge amount of requests/processing(s) at a time.

- **To make service handle multi-sourced or multi-structured entities out of the box, canonical data model is brought into the picture. That means in system there is a Data Dictionary defined where each attribute holds agreed-upon meaning. Each entity within the system has a common representation/structure that all the services within the system are expected to consume and return as an standard format. The goal of canonical model is to provide a dictionary of re-usable common objects/definitions at an enterprise of business-domain level to enhance system interoperability.**

- **Canonical services loads configuration or mappings from cache and executes the rule to transform the received data into the canonical format i.e. whenever any new entity is expected to be processed successfully its configuration/mapping file is expected to be loaded beforehand.**

- As per need Elasticsearch Percolator feature best fits here, it lets you reverse search by storing search queries as documents in an index. Then the percolate query can be used to match queries stored in an index as new listings arrive. Elasticsearch can be scaled as per needs out of the box, just load needs to be tested to decide configuration.

- Listing topic is fed new listings on arrival which are consumed by (1) Listing Indices and it'll also persist in other stores as well but that's not covered here.. and (2) Canonical Service that pushes data to Canonical Topic which signals to execute Perlocate Processor and in case of matching hits Saved Search Topic is fed and consumed by Saved Search Service that then signals Notification Service to take care of sending alerts to users.

- Saved Search service is also responsible to persist user saved-searches in the no-sql database.

- **Orange box shows that Saved Search service is an isolated piece, it just expects the input in canonical format from any source. It also exposes APIs for CRUD.**

- Subscription service takes care of subscriptions i.e. by default each newly created saved search has enabled subscription but later user can disable it to avoid notifications whilst saved-search settings will remain intact

- Config service lets system user define and manage configuration and mapping templates.

- Notification Service is kept direct now but can topic interfaced as well.
  
### Scaling ###

- If cloud services are used like aws/azure computes then auto-scaling is just a configuration away, yep it's that simple.

- Or per micro-service proper resource requirement can be decided based on cpu, memory utilization and traffic handling like peak vs normal. Then horizontal scaling can be achieved with load-balancing technique achieved via kubernetes. 

### REST APIs ###

- GET /v1/saved-search?skip=:skip&limit=:limit
    - List all the saved searches of authenticated user.

- GET /v1/saved-search/:id
    - Fetch particular saved search by provided id for authenticated user.

- POST /v1/saved-search
     - Create a new saved search with provided body payload for authenticated user.

- PUT /v1/saved-search/:id
    - Update saved search entry with provided body payload by provided id for authenticated user.
 
- DELETE /v1/saved-search/:id
    - Delete saved search entry by provided id for authenticated user.


### Conclusive Note ###

Points are tried to be better explained but in case of any query or ambiguous statement I would still love to reply back via current communication channel..

